
Hi!

This single page web app uses the following libraries and frameworks

- vue
- axios
- bootstrap 4

- Part of the axios request, a catch function is defined to render an error message on the page in-case an issue has occured.
- a search() function is triggered for every 'Enter' keypress from the search field
- the list of results are rendered with bootstrap media list