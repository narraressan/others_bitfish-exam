
let app = new Vue({
	el: '#app',
	data: {
		searchResults: [],
		keyword: '',
		message: {
			error: false,
			text: ''
		}
	},
	computed: {
		pages: function(){
			return 'pages'
		}
	},
	methods: {
		search: function() {
			// Reset messages and previously loaded results
			this.searchResults = []
			this.message.error = false
			this.message.text = ''

			axios({
				method: 'GET',
				url: 'https://www.headlightlabs.com/api/assessment_search_wrapper',
				params: {
					// clean keyword format
					query: this.keyword.replace(/ /g, '+'),
					limit: 10,
					offset: 0
				}
			})
			.then((response) => {
				let rawData = response.data
				this.searchResults = rawData.itemListElement

				if (this.searchResults.length <= 0) {
					this.message.error = true
					this.message.text = 'No Results Found'
				}
				else {
					this.message.error = false
					this.message.text = this.searchResults.length + ' Results Found'
				}
			})
			.catch((error) => {
				this.message.error = true
				this.message.text = 'Request Failed'
			})
		}
	},
	mounted() {
		console.log('App Init!')
	}
})